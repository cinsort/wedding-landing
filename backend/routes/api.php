<?php

declare(strict_types=1);

use App\Http\Controllers\ListGuestAction;
use App\Http\Controllers\UpdateGuestAction;
use Illuminate\Support\Facades\Route;

Route::prefix('/guests')->name('guests.')->group(function (): void {
    Route::get('/', ListGuestAction::class)->name('list');
    Route::put('/{guest}', UpdateGuestAction::class)->name('update');
});
