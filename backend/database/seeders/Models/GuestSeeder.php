<?php

declare(strict_types=1);

namespace Database\Seeders\Models;

use App\Models\Guest;
use Illuminate\Database\Seeder;

class GuestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Guest::factory()->count(10)->create();
    }
}
