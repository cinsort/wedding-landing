<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Enums\GuestInvitationStatus;
use App\Models\Guest;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Guest>
 */
class GuestFactory extends Factory
{
    public function definition(): array
    {
        return [
            'name' => $this->faker->name(),
            'description' => $this->faker->realText(1000),
            'invitation_status' => $this->faker->randomElement(GuestInvitationStatus::cases()),
        ];
    }
}
