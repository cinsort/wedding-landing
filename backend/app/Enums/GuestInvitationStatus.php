<?php

declare(strict_types=1);

namespace App\Enums;

use Filament\Support\Contracts\HasColor;
use Filament\Support\Contracts\HasLabel;

enum GuestInvitationStatus: string implements HasLabel, HasColor
{
    case CREATED = 'created';
    case ACCEPTED = 'accepted';
    case REJECTED = 'rejected';

    public function transitionMap(): array
    {
        return match ($this) {
            self::CREATED => [
                self::CREATED,
                self::ACCEPTED,
                self::REJECTED,
            ],
            self::ACCEPTED => [
                self::REJECTED,
            ],
            self::REJECTED => [
                self::ACCEPTED,
            ]
        };

    }

    public function getLabel(): ?string
    {
        return match ($this) {
            self::CREATED => 'Создано',
            self::ACCEPTED => 'Принято',
            self::REJECTED => 'Отклонено',
        };
    }

    public function getColor(): string|array|null
    {
        return match ($this) {
            self::CREATED => 'gray',
            self::ACCEPTED => 'success',
            self::REJECTED => 'warning',
        };
    }
}
