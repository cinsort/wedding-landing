<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\UpdateGuestRequest;
use App\Http\Resources\GuestResource;
use App\Models\Guest;
use Illuminate\Routing\Controller;

class UpdateGuestAction extends Controller
{

    public function __invoke(UpdateGuestRequest $request, Guest $guest): GuestResource
    {
        $request->validateInvitationStatus($guest);
        $guest->update($request->validated());
        return GuestResource::make($guest);
    }
}
