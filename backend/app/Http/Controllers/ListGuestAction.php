<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Resources\GuestResource;
use App\Models\Guest;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Routing\Controller;

class ListGuestAction extends Controller
{
    public function __invoke(Request $request): AnonymousResourceCollection
    {
        $guests = Guest::all();
        return GuestResource::collection($guests);
    }
}
