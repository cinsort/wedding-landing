<?php

declare(strict_types=1);

namespace App\Http\Requests;

use App\Enums\GuestInvitationStatus;
use App\Models\Guest;
use App\Rules\GuestInvitationStatusTransitionRule;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateGuestRequest extends FormRequest
{
    /**
     * @return array<string, ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => [
                'sometimes',
                'required',
                'string',
                'max:255',
            ],
            'invitation_status' => [
                'sometimes',
                'required',
                'string',
                Rule::enum(GuestInvitationStatus::class),
            ]
        ];
    }

    public function name(): string
    {
        return $this->input('name');
    }

    public function invitationStatus(): GuestInvitationStatus
    {
        return GuestInvitationStatus::tryFrom($this->input('invitation_status'));
    }

    public function validateInvitationStatus(Guest $guest): void
    {
        $this->validate([
            'invitation_status' => [new GuestInvitationStatusTransitionRule($guest)]
        ]);
    }

    public function bodyParameters(): array
    {
        return [
            'name' => [
                'description' => 'Имя гостя',
                'example' => 'Вадим',
            ],
            'invitation_status' => [
                'description' => 'Статус приглашения',
                'example' => 'accepted',
            ],
        ];
    }
}
