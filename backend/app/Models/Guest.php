<?php

declare(strict_types=1);

namespace App\Models;

use App\Enums\GuestInvitationStatus;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $description
 * @property GuestInvitationStatus $invitation_status
 */
class Guest extends Model
{
    use HasFactory;

    /**
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'invitation_status',
        'description',
    ];

    /**
     * @var array<int, string>
     */
    protected $casts = [
        'invitation_status' => GuestInvitationStatus::class,
    ];
}
