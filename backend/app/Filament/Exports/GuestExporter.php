<?php

declare(strict_types=1);

namespace App\Filament\Exports;

use App\Enums\GuestInvitationStatus;
use App\Models\Guest;
use Filament\Actions\Exports\Enums\ExportFormat;
use Filament\Actions\Exports\ExportColumn;
use Filament\Actions\Exports\Exporter;
use Filament\Actions\Exports\Models\Export;
use Illuminate\Database\Eloquent\Builder;

class GuestExporter extends Exporter
{
    protected static ?string $model = Guest::class;

    public static function getColumns(): array
    {
        return [
            ExportColumn::make('name')
                ->label('Имя'),
            ExportColumn::make('description')
                ->label('Описание'),
        ];
    }

    public static function modifyQuery(Builder $query): Builder
    {
        return $query->where('invitation_status', '!=', GuestInvitationStatus::REJECTED);
    }

    public function getFormats(): array
    {
        return [
            ExportFormat::Xlsx,
        ];
    }

    public static function getCompletedNotificationBody(Export $export): string
    {
        $body = 'Your guest export has completed and '
            . number_format($export->successful_rows)
            . ' '
            . str('row')->plural($export->successful_rows)
            . ' exported.';

        if ($failedRowsCount = $export->getFailedRowsCount()) {
            $body .= ' '
                . number_format($failedRowsCount)
                . ' ' . str('row')->plural($failedRowsCount)
                . ' failed to export.';
        }

        return $body;
    }
}
