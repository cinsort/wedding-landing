<?php

declare(strict_types=1);

namespace App\Filament\Resources\GuestResource\Pages;

use App\Enums\GuestInvitationStatus;
use App\Filament\Exports\GuestExporter;
use App\Filament\Resources\GuestResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;
use Illuminate\Database\Eloquent\Builder;

class ListGuests extends ListRecords
{
    protected static string $resource = GuestResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
            Actions\ExportAction::make()
                ->exporter(GuestExporter::class),
        ];
    }
}
