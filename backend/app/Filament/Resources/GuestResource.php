<?php

declare(strict_types=1);

namespace App\Filament\Resources;

use App\Enums\GuestInvitationStatus;
use App\Filament\Resources\GuestResource\Pages;
use App\Models\Guest;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Str;

class GuestResource extends Resource
{
    protected static ?string $model = Guest::class;

    protected static ?string $navigationIcon = 'heroicon-m-user';
    protected static ?string $label = 'Гость';
    protected static ?string $pluralLabel = 'Гости';
    protected static ?string $modelLabel = 'Гостя';
    protected static ?string $pluralModelLabel = 'Гости';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('name')
                    ->label('Имя')
                    ->required()
                    ->maxLength(100),
                Forms\Components\TextInput::make('description')
                    ->label('Описание')
                    ->nullable()
                    ->maxLength(1000),
                Forms\Components\Select::make('invitation_status')
                    ->label('Статус приглашения')
                    ->default(GuestInvitationStatus::CREATED)
                    ->required()
                    ->options(GuestInvitationStatus::class)
                    ->enum(GuestInvitationStatus::class),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('name')
                    ->label('Имя')
                    ->searchable(),
                Tables\Columns\TextColumn::make('description')
                    ->label('Описание')
                    ->formatStateUsing(fn ($state): string => Str::limit($state, 30)),
                Tables\Columns\TextColumn::make('invitation_status')
                    ->label('Статус приглашения')
                    ->summarize(
                        [
                            Tables\Columns\Summarizers\Count::make()
                                ->label('Принявших приглашение')
                                ->query(fn (Builder $query) => $query->where(
                                    'invitation_status',
                                    GuestInvitationStatus::ACCEPTED
                                )),
                            Tables\Columns\Summarizers\Count::make()
                                ->label('Всего не отклонивших приглашение')
                                ->query(fn (Builder $query) => $query->whereIn(
                                    'invitation_status',
                                    [GuestInvitationStatus::CREATED, GuestInvitationStatus::ACCEPTED]
                                )),
                            Tables\Columns\Summarizers\Count::make('id')
                                ->label('Всего гостей'),
                        ]
                    ),
                Tables\Columns\TextColumn::make('created_at')
                    ->label('Дата создания')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('updated_at')
                    ->label('Дата изменения')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: false),
            ])
            ->actions([
                Tables\Actions\EditAction::make()->hiddenLabel(),
                Tables\Actions\DeleteAction::make()->hiddenLabel(),
            ]);
    }

    public static function getRelations(): array
    {
        return [];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListGuests::route('/'),
        ];
    }
}
