<?php

declare(strict_types=1);

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DebugCommand extends Command
{
    protected $signature = 'debug';

    protected $description = 'Debug application functionality';

    public function handle()
    {
        //
    }
}
