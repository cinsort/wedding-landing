<?php

declare(strict_types=1);

namespace App\Rules;

use App\Enums\GuestInvitationStatus;
use App\Models\Guest;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class GuestInvitationStatusTransitionRule implements ValidationRule
{
    public function __construct(protected Guest $guest)
    {
    }

    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $value = GuestInvitationStatus::tryFrom($value);

        if (! in_array($value, $this->guest->invitation_status->transitionMap())) {
            $fail('The :attribute transition is not allowed.');
        }
    }
}
