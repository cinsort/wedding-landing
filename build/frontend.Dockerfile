FROM node:20-alpine3.19 as builder

ARG VITE_API_URL

WORKDIR /app
COPY package*.json .
RUN npm install
COPY . .
RUN echo "VITE_API_URL=${VITE_API_URL}" > .env;
RUN npm run build

FROM builder as server-local

CMD ["npm", "run", "dev"]

FROM nginxinc/nginx-unprivileged:1.26.0-perl as server

ARG UID=1000
ARG GID=1000

USER $UID

COPY --chown=$UID:$GID nginx.conf /etc/nginx/conf.d/default.conf
COPY --chown=$UID:$GID --from=builder /app/dist /usr/share/nginx/html
EXPOSE 3000
CMD ["nginx", "-g", "daemon off;"]
