FROM nginxinc/nginx-unprivileged:1.26.0-perl

ARG UID=1000
ARG GID=1000

WORKDIR /app

RUN rm -rf /etc/nginx/conf.d/default.conf
COPY --chown=$UID:$GID ./nginx.conf /etc/nginx/conf.d/nginx.conf

USER $UID
