DOCKER_COMPOSE:=docker compose

up-d:
	$(DOCKER_COMPOSE) up --detach

backend-sh:
	$(DOCKER_COMPOSE) exec backend sh

build-all:
	$(DOCKER_COMPOSE) build
build-all-plain:
	$(DOCKER_COMPOSE) --progress=plain build
build-all-no-cache:
	$(DOCKER_COMPOSE) --progress=plain build --no-cache

user:
	$(DOCKER_COMPOSE) exec backend php artisan make:filament-user
