import { ApiCore } from "@/api/core/ApiCore";

export enum InvitationStatus {
    CREATED = 'created',
    ACCEPTED = 'accepted',
    REJECTED = 'rejected'
}

export interface IGuest {
    id: number,
    name: string,
    invitation_status: InvitationStatus
}

class GuestApi extends ApiCore {
    public getGuestList(): Promise<IGuest[]> {
        return this.GET('/guests');
    };
    public changeInvitationStatus(guestID: number, status: InvitationStatus): Promise<unknown> {
        return this.PUT(`/guests/${guestID}`, {
            invitation_status: status,
        })
    };
}

export const GuestAPI = new GuestApi();
