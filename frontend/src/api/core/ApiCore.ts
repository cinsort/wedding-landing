enum RequestMethod {
    GET = 'GET',
    PUT = 'PUT',
}

export interface IRequestOptions {
    method: RequestMethod,
}
export interface IRequestBody {}
export interface IRequestParams {}

export interface IRequestResponse<T> {
    data: T
}

export interface IApiCore {
    GET: (url: string, params: IRequestParams) => Promise<any>,
    PUT: (url: string, body: IRequestBody) => Promise<any>,
}

export class ApiCore implements IApiCore {
    private readonly _baseUrl = import.meta.env.VITE_API_URL;

    public async GET<ResponseType>(url: string, params?: IRequestParams) {
        const response = await this._createRequest(
            url,
            {
                method: RequestMethod.GET,
            },
            undefined,
            params
        );

        return this._unwrapResponse<ResponseType>(response);
    }

    public async PUT<ResponseType>(url: string, body?: IRequestBody) {
        const response = await this._createRequest(
            url,
            {
                method: RequestMethod.PUT,
            },
            body,
        );

        return this._unwrapResponse<ResponseType>(response)
    }

    private _createRequest(url: string, options: IRequestOptions, body?: IRequestBody, params?: IRequestParams) {
        return fetch(`${this._baseUrl}${url}`, {
            headers: {
                'Content-Type': 'application/json',
            },
            ...options,
            body: JSON.stringify(body),
        }).then((response) => response.json())
    }

    private _unwrapResponse<T>(response: IRequestResponse<T>): T {
        return response.data
    }
}
