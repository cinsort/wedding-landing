1. In order to update project, create issue and correspondent branch
    1. Issue name must follow pattern: "<Change level>: <Short description> (<planned version tag>)"
    Available change levels:
       * Feature: new logic
       * Fix: correction logic
       * Improvement: enhancement or refactoring logic
       * Remove: cleanup logic
    2. Issue description must contain full changes list with shorten description
2. Commit message must follow pattern: "<Change level>: <Short description>"
3. Upon completion of development, open the merge request associated with the issue
   1. Only maintainers are able to merge into main
   2. Source branch must be deleted
   3. Merge request requires single approve before merging
