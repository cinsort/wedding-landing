# Wedding-landing

## Launch of the project

1. Create files with environment variables
 - For backend service: copy `./backend/.env.example` to new file `./backend/.env`
 - For docker-compose: copy `.env.example` to new file `.env`

2. Execute command to run project services:
   ```shell
    docker compose up -d
    ```

3. Make sure all services are running successfully:
   ```shell
    docker compose ps
   ```
   
4. Project components should be available at `http://localhost:80`

## Working with service from console:

1. If necessary, execute a console command in service:
   ```shell
    docker compose exec <SERVICE_NAME> <COMMAND_NAME> <COMMAND_ARGUMENTS>
   ```
 - when you need to start service without dependencies:
   ```shell
    docker compose run --rm --no-deps <SERVICE_NAME> <COMMAND_NAME> <COMMAND_ARGUMENTS>
   ```
 - when it is necessary to start service along with dependencies (database, redis)
   ```shell
    docker compose run --rm <SERVICE_NAME> <COMMAND_NAME> <COMMAND_ARGUMENTS>
   ```

## Auxiliary commands

> All `docker compose` commands must be run from the root of the project

### Rebuilding service without cache

```shell
docker compose --progress=plain build --no-cache <SERVICE_NAME>
```

### View the resulting docker-compose configuration file for an individual service

```shell
docker compose config <SERVICE_NAME>
```
